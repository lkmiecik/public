lazy val root = (project in file(".")).
  settings(
    inThisBuild(List(
      organization := "pl.lkmiecik",
      scalaVersion := "2.12.4"
    )),
    name := "margincalculator"
  )

libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % Test
