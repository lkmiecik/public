package pl.lkmiecik.finpack.service

import pl.lkmiecik.finpack.model.OwnContributionLowerBound
import pl.lkmiecik.finpack.model.LownPeriodLowerBound
import pl.lkmiecik.finpack.model.PLN
import pl.lkmiecik.finpack.model.Percent
import pl.lkmiecik.finpack.model.ColumnForAmountInterval

class MarginCalculatorConstructionDataTest extends org.scalatest.FlatSpec {
  
  def correctValues = {
    new {
      val boundInMonths0 = 1
      val amountBounds0: List[PLN] = List(0).map(PLN(_))
      val ownContributionBounds0: List[Percent] = List(10, 20).map(Percent(_))
      val marginTable0: List[ColumnForAmountInterval] = List(ColumnForAmountInterval(2.94, 2.67))
                                                  
      val boundInMonths1 = 13
      val amountBounds1: List[PLN] = List(0, 40000).map(PLN(_))
      val ownContributionBounds1: List[Percent] = List(10, 20, 30).map(Percent(_))
      val marginTable1: List[ColumnForAmountInterval] = List(ColumnForAmountInterval(3.94, 3.67, 3.54),
                                                         ColumnForAmountInterval(2.49, 2.32, 2.29))
    }
  }
  
  "Correct entry data" should "result in creating Right[String, MarginCalculator]" in {
    val calculator0 = MarginCalculator(
        correctValues.boundInMonths0, 
        correctValues.marginTable0, 
        correctValues.amountBounds0, 
        correctValues.ownContributionBounds0)
    val calculator = calculator0.flatMap(c => {
      c.addNextPeriod(
          correctValues.boundInMonths1, 
          correctValues.marginTable1, 
          correctValues.amountBounds1, 
          correctValues.ownContributionBounds1)
    })
    assert(calculator.isInstanceOf[Right[String, MarginCalculator]])
  }
  
  "Negative margin" should "reuslt in creating Left(Margin should not be less than 0: ...)" in {
    val marginTableWithNegativeValue: List[ColumnForAmountInterval] = List(ColumnForAmountInterval(2.94, -1))
    val calculator = MarginCalculator(
        correctValues.boundInMonths0, 
        marginTableWithNegativeValue, 
        correctValues.amountBounds0, 
        correctValues.ownContributionBounds0)
    assert(calculator.left.get.startsWith("Margin should not be less than 0"))
  }
  
  "Negetive own contribution bound" should "result in creating Left(Own contribution lower bound should not be less than 0: ...)" in {
    val ownContributionBoundsWithNegetiveValues: List[Percent] = List(-10, 20, 30).map(Percent(_))
    val calculator = MarginCalculator(
        correctValues.boundInMonths1, 
        correctValues.marginTable1, 
        correctValues.amountBounds1, 
        ownContributionBoundsWithNegetiveValues)
    assert(calculator.left.get.startsWith("Own contribution lower bound should not be less than 0"))
  }
  
  "Own contribution bound greater than 100" should "result in creating Left(Own contribution lower bound should not be greater or equal 100: ...)" in {
    val ownContributionBoundsWithNegetiveValues: List[Percent] = List(20, 30, 120).map(Percent(_))
    val calculator = MarginCalculator(
        correctValues.boundInMonths1, 
        correctValues.marginTable1, 
        correctValues.amountBounds1, 
        ownContributionBoundsWithNegetiveValues)
    assert(calculator.left.get.startsWith("Own contribution lower bound should not be greater or equal 100"))
  }
  
  
  "Repeated own contribution bounds" should "result in creating Left(Own contribution bounds values should not repeat)" in {
    val repeatedOwnContributionBounds: List[Percent] = List(20, 30, 30).map(Percent(_))
    val calculator = MarginCalculator(
        correctValues.boundInMonths1, 
        correctValues.marginTable1, 
        correctValues.amountBounds1, 
        repeatedOwnContributionBounds)
    assert(calculator.left.get.startsWith("Own contribution bounds values should not repeat"))
  }
  
  "Lown amount lower bound less than 0" should "result in creating Left(Lown amount lower bound should not be less than 0: ...)" in {
    val amountBoundsWithNegetiveValues: List[PLN] = List(-40000, 0).map(PLN(_))
    val calculator = MarginCalculator(
        correctValues.boundInMonths1, 
        correctValues.marginTable1, 
        amountBoundsWithNegetiveValues, 
        correctValues.ownContributionBounds1)
    assert(calculator.left.get.startsWith("Lown amount lower bound should not be less than 0"))
  }

  "Empty amount bounds list" should "result in creating Left(Amount bounds list should not be empty)" in {
    val emptyAmountBounds: List[PLN] = List()
    val calculator = MarginCalculator(
        correctValues.boundInMonths1, 
        correctValues.marginTable1, 
        emptyAmountBounds, 
        correctValues.ownContributionBounds1)
    assert(calculator.left.get.startsWith("Amount bounds list should not be empty"))
  }
  
  "Repeated amount bounds values" should "result in creating Left(Amount bounds values should not repeat)" in {
    val repeatedAmountBoundsBounds: List[PLN] = List(40000, 40000).map(PLN(_))
    val calculator = MarginCalculator(
        correctValues.boundInMonths1, 
        correctValues.marginTable1, 
        repeatedAmountBoundsBounds, 
        correctValues.ownContributionBounds1)
    assert(calculator.left.get.startsWith("Amount bounds values should not repeat"))
  }
  
  "Margin table size and own contribution list size do not match" should "result in creating Left(Margins list size and own contribution list size not the same)" in {
    val tooLongOwnContributionBounds: List[Percent] = List(20, 50, 80, 90).map(Percent(_))
    val calculator = MarginCalculator(
        correctValues.boundInMonths1, 
        correctValues.marginTable1, 
        correctValues.amountBounds1, 
        tooLongOwnContributionBounds)
    assert(calculator.left.get.startsWith("Margins list size and own contribution list size not the same"))
  }
  
  "Margin table columns has not the same length" should "result in creating Left(Margin table columns has not the same length or table is empty)" in {
    val marginTableWithDifferentRowLengts: List[ColumnForAmountInterval] = 
      List(ColumnForAmountInterval(3.94, 3.67, 3.54),
           ColumnForAmountInterval(2.49, 2.32))
    val calculator = MarginCalculator(
        correctValues.boundInMonths1, 
        marginTableWithDifferentRowLengts, 
        correctValues.amountBounds1, 
        correctValues.ownContributionBounds1)
    assert(calculator.left.get.startsWith("Margin table columns has not the same length or table is empty"))
  }
  
  "Margin table is Empty" should "result in creating Left(Margin table columns has not the same length or table is empty)" in {
    val emptyMarginTable: List[ColumnForAmountInterval] = List()
    val calculator = MarginCalculator(
        correctValues.boundInMonths1, 
        emptyMarginTable, 
        correctValues.amountBounds1, 
        correctValues.ownContributionBounds1)
    assert(calculator.left.get.startsWith("Margin table columns has not the same length or table is empty"))
  }
  
  "Empty own contribution list" should "result in creating Left(Own contribution list should not be empty)" in {
    val emptyOwnContributionBounds: List[Percent] = List()
    val calculator = MarginCalculator(
        correctValues.boundInMonths1, 
        correctValues.marginTable1, 
        correctValues.amountBounds1, 
        emptyOwnContributionBounds)
    assert(calculator.left.get.startsWith("Own contribution list should not be empty"))
  }
  
  "Bound in months less than 1" should "result in creating Left(Bound in months should not be less than 1: ...)" in {
    val boundInMontLessThan1 = 0
    val calculator = MarginCalculator(
        boundInMontLessThan1, 
        correctValues.marginTable0, 
        correctValues.amountBounds0, 
        correctValues.ownContributionBounds0)
    assert(calculator.left.get.startsWith("Bound in months should not be less than 1"))
  }
  
  "Not sorted amount bounds" should "result in creating Left(Amount bounds list is not sorted: ...)" in {
    val notSortedAmountBounds: List[PLN] = List(40000, 0).map(PLN(_))
    val calculator = MarginCalculator(
        correctValues.boundInMonths1, 
        correctValues.marginTable1, 
        notSortedAmountBounds, 
        correctValues.ownContributionBounds1)
    assert(calculator.left.get.startsWith("Amount bounds list is not sorted"))
  }
  
  "Not sorted own contribution bounds" should "result in creating Left(Own contribution bounds list is not sorted: ...)" in {
    val notSortedOwnContributionBounds: List[Percent] = List(20, 30, 10).map(Percent(_))
    val calculator = MarginCalculator(
        correctValues.boundInMonths1, 
        correctValues.marginTable1, 
        correctValues.amountBounds1, 
        notSortedOwnContributionBounds)
    assert(calculator.left.get.startsWith("Own contribution bounds list is not sorted"))
  }
  
  "Bounds in months repeaded" should "result in creating Left(Bounds in months should be different with one another)" in {
    val calculator0 = MarginCalculator(
        correctValues.boundInMonths0, 
        correctValues.marginTable0, 
        correctValues.amountBounds0, 
        correctValues.ownContributionBounds0)
    val calculator = calculator0.flatMap(c => {
      c.addNextPeriod(
          correctValues.boundInMonths0,
          correctValues.marginTable1, 
          correctValues.amountBounds1, 
          correctValues.ownContributionBounds1)
    })
    assert(calculator.left.get.startsWith("Bounds in months should be different with one another"))
  }
  
  
}