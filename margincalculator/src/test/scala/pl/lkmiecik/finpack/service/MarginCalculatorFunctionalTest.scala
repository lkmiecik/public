package pl.lkmiecik.finpack.service

import pl.lkmiecik.finpack.model.OwnContributionLowerBound
import pl.lkmiecik.finpack.model.LownPeriodLowerBound
import pl.lkmiecik.finpack.model.PLN
import pl.lkmiecik.finpack.model.Percent
import pl.lkmiecik.finpack.model.ColumnForAmountInterval

class MarginCalculatorFunctionalTest extends org.scalatest.FlatSpec {
  
  def valuesFromPdf = {
    new {
      val boundInMonths0 = 1
      val amountBounds0: List[PLN] = List(PLN(0))
      val ownContributionBounds0: List[Percent] = List(Percent(0))
      val marginTable0: List[ColumnForAmountInterval] = List(ColumnForAmountInterval(1.1))
      
      val boundInMonths1 = 13
      val amountBounds1: List[PLN] = List(PLN(0), PLN(40000), PLN(80000), PLN(120000), PLN(200000), PLN(700000))
      val ownContributionBounds1: List[Percent] = List(Percent(10), Percent(20), Percent(30), Percent(50))
      val marginTable1: List[ColumnForAmountInterval] = List(ColumnForAmountInterval(3.94, 3.67, 3.54, 2.94),
                                                         ColumnForAmountInterval(2.49, 2.32, 2.29, 2.18),
                                                         ColumnForAmountInterval(2.17, 2.00, 1.97, 1.91),
                                                         ColumnForAmountInterval(2.05, 1.88, 1.85, 1.79),
                                                         ColumnForAmountInterval(1.83, 1.66, 1.65, 1.55),
                                                         ColumnForAmountInterval(1.74, 1.55, 1.55, 1.45))
    }
  }
  
  def getCalculator(): MarginCalculator = {
    val calculator = for {
      calculator0 <- MarginCalculator(
        valuesFromPdf.boundInMonths0, 
        valuesFromPdf.marginTable0, 
        valuesFromPdf.amountBounds0, 
        valuesFromPdf.ownContributionBounds0)
        
      calculator1 <- calculator0.addNextPeriod(
        valuesFromPdf.boundInMonths1, 
        valuesFromPdf.marginTable1, 
        valuesFromPdf.amountBounds1, 
        valuesFromPdf.ownContributionBounds1)
        
    } yield calculator1
    
    calculator.right.getOrElse(new MarginCalculator)
  }
  
  
  "If month value less or equal 0 then margin" should "not be calculated" in {
    val calculator: MarginCalculator = getCalculator()
    assert(calculator.calculate(0, PLN(30000), Percent(20)) === None)
    assert(calculator.calculate(-10, PLN(30000), Percent(20)) === None)
  }
  
  "If lown amount less or equal 0 then margin" should "not be calculated" in {
    val calculator: MarginCalculator = getCalculator()
    assert(calculator.calculate(1, PLN(0), Percent(50)) === None)
    assert(calculator.calculate(6, PLN(-100000), Percent(50)) === None)
  }
  
  "If own contribution less than 0 then margin" should "not be calculated" in {
    val calculator: MarginCalculator = getCalculator()
    assert(calculator.calculate(1, PLN(30000), Percent(-10)) === None)
    assert(calculator.calculate(6, PLN(100000), Percent(-50)) === None)
  }
  
  "If own contribution greater or equal 100 then margin" should "not be calculated" in {
    val calculator: MarginCalculator = getCalculator()
    assert(calculator.calculate(1, PLN(30000), Percent(110)) === None)
    assert(calculator.calculate(6, PLN(100000), Percent(150)) === None)
  }
  
  "During period from 1st month to 12th month calculated margin" should "always equals 1.1" in {
    val calculator: MarginCalculator = getCalculator()
    assert(calculator.calculate(1, PLN(30000), Percent(20)) === Some(1.1))
    assert(calculator.calculate(12, PLN(30000), Percent(20)) === Some(1.1))
    assert(calculator.calculate(1, PLN(800000), Percent(50)) === Some(1.1))
    assert(calculator.calculate(6, PLN(800000), Percent(50)) === Some(1.1))
    assert(calculator.calculate(6, PLN(500000), Percent(30)) === Some(1.1))
    assert(calculator.calculate(12, PLN(500000), Percent(30)) === Some(1.1))
  }
  
  "During period after 12th month margin" should "not be calculated if own contribution is less than 10 percent" in {
    val calculator: MarginCalculator = getCalculator()
    assert(calculator.calculate(13, PLN(30000), Percent(0)) === None)
    assert(calculator.calculate(62, PLN(100000), Percent(9)) === None)
  }
  
  "Lower amount bound" should "be included to the interval" in {
    val calculator: MarginCalculator = getCalculator()
    assert(calculator.calculate(23, PLN(40000), Percent(25)) === Some(2.32))
    assert(calculator.calculate(23, PLN(200000), Percent(45)) === Some(1.65))
    assert(calculator.calculate(23, PLN(120000), Percent(65)) === Some(1.79))
  }
  
  "Own contribution lower bound" should "be included to the interval" in {
    val calculator: MarginCalculator = getCalculator()
    assert(calculator.calculate(23, PLN(45000), Percent(30)) === Some(2.29))
    assert(calculator.calculate(23, PLN(95000), Percent(20)) === Some(2.00))
    assert(calculator.calculate(23, PLN(700000), Percent(10)) === Some(1.74))
  }
  
  "Calculator" should "calculate values form 5th column from table in pdf when lown amount is between 200000 and 700000" in {
    val calculator: MarginCalculator = getCalculator()
    assert(calculator.calculate(60, PLN(400000), Percent(10)) === Some(1.83))
    assert(calculator.calculate(60, PLN(300000), Percent(15)) === Some(1.83))
    assert(calculator.calculate(60, PLN(500000), Percent(20)) === Some(1.66))
    assert(calculator.calculate(60, PLN(400000), Percent(25)) === Some(1.66))
    assert(calculator.calculate(60, PLN(600000), Percent(30)) === Some(1.65))
    assert(calculator.calculate(60, PLN(680000), Percent(45)) === Some(1.65))
    assert(calculator.calculate(60, PLN(250000), Percent(50)) === Some(1.55))
    assert(calculator.calculate(60, PLN(550000), Percent(75)) === Some(1.55))
  }
  
}