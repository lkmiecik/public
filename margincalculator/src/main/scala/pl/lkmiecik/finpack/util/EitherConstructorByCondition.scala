package pl.lkmiecik.finpack.util

import scala.Left
import scala.Right

trait EitherConstructorByCondition {
  
  def either(condition: Boolean, failureInfo: String): Either[String, Boolean] = {
    if (condition) Right(true)
    else Left(failureInfo)
  }
  
}