package pl.lkmiecik.finpack.model

case class ColumnForAmountInterval(val column: List[Double]) {}

object ColumnForAmountInterval {
  def apply(xs: Double*): ColumnForAmountInterval = new ColumnForAmountInterval(xs.toList)
}