package pl.lkmiecik.finpack.model

import pl.lkmiecik.finpack.util.EitherConstructorByCondition

case class LownPeriodLowerBound private(val boundInMonths: Int, private val lownAmountLowerBoundsList: List[LownAmountLowerBound]) {
  
  val lownAmountLowerBounds: Option[List[LownAmountLowerBound]] = {
    Option(lownAmountLowerBoundsList.sortBy(-_.bound))
  }
  
}

object LownPeriodLowerBound extends EitherConstructorByCondition {
  
  def apply(boundInMonths: Int, lownAmountLowerBoundsList: List[LownAmountLowerBound]): Nothing = {
    throw new IllegalArgumentException("Use different construction arguments")
  }
  
  def apply(boundInMonths: Int, marginColumns: List[ColumnForAmountInterval], 
      amountBoundsPLN: List[PLN], ownContributionBoundsPercent: List[Percent]): Either[String, LownPeriodLowerBound] = {
    
    val amountBounds = amountBoundsPLN.map(_.value)
    val ownContributionBounds = ownContributionBoundsPercent.map(_.value)
    val marginTable = marginColumns.map(_.column)
    for {
      c1 <- either(boundInMonths >= 1, 
          "Bound in months should not be less than 1: " + boundInMonths)
      c2 <- either(!amountBounds.isEmpty,
          "Amount bounds list should not be empty")
      c3 <- either(boundsAreNotRepeated(amountBounds), 
          "Amount bounds values should not repeat")
      c4 <- either(boundsAreNotRepeated(ownContributionBounds),
          "Own contribution bounds values should not repeat")
      c5 <- either(isMarginTableRowsSizesAreTheSameAndGreaterThan0(marginTable),
          "Margin table columns has not the same length or table is empty")
      c6 <- either(marginTable.size == amountBounds.size,
          "Margins list size and amount bound list size not the same: " + marginTable.size + " != " + amountBounds.size)
      c7 <- either(isSorted(amountBounds),
          "Amount bounds list is not sorted")
      c8 <- either(isSorted(ownContributionBounds),
          "Own contribution bounds list is not sorted")
      lownAmountLowerBoundsList <- listOfEitherToEitherList(marginTable.zip(amountBounds).map(t => LownAmountLowerBound(t._2, t._1, ownContributionBounds)))
    } yield new LownPeriodLowerBound(boundInMonths, lownAmountLowerBoundsList)
  }
  
  private def boundsAreNotRepeated(bounds: List[Double], value: Option[Double] = Option.empty): Boolean = bounds match {
    case List() => true
    case List(x) => value.map(v => x != v).getOrElse(true)
    case x :: xs => {
      value.map(v => x != v).getOrElse(true) && 
      boundsAreNotRepeated(xs, Option(x)) && 
      value.map(v => boundsAreNotRepeated(xs, Option(v))).getOrElse(true) 
    }
  }
  
  private def isMarginTableRowsSizesAreTheSameAndGreaterThan0(marginTable: List[List[Double]]): Boolean = {
    val sizes = marginTable.map(_.size)
    allSizesTheSameAndGreaterThanZero(sizes)
  }
  
  private def allSizesTheSameAndGreaterThanZero(sizes: List[Int]): Boolean = sizes match {
    case List() => false
    case List(x) => x > 0
    case list => {
      list.reduce((v1, v2) => {
        if (v1 == v2) {
          v1
        } else {
          0
        }
      }) > 0
    }
  }
  
  private def listOfEitherToEitherList(list: List[Either[String, LownAmountLowerBound]]): Either[String, List[LownAmountLowerBound]] = {
    list.partition(_.isLeft) match {
      case (Nil, ownBounds) => Right(for(Right(bound) <- ownBounds) yield bound)
      case (strings, _) => Left((for{Left(s) <- strings} yield s).head)
    }
  }
  
  private def isSorted(l: List[Double]): Boolean = l == l.sorted
}