package pl.lkmiecik.finpack.model

case class Percent(val value: Double) {}