package pl.lkmiecik.finpack.model

import pl.lkmiecik.finpack.util.EitherConstructorByCondition

case class OwnContributionLowerBound private(val bound: Double, val margin: Double) {
}

object OwnContributionLowerBound extends EitherConstructorByCondition {
  
  def apply(bound: Double, margin: Double): Either[String, OwnContributionLowerBound] = {
    for {
      c1 <- either(bound >= 0,
          "Own contribution lower bound should not be less than 0: " + bound)
      c2 <- either(bound < 100,
          "Own contribution lower bound should not be greater or equal 100: " + bound)
      c3 <- either(margin >= 0,
          "Margin should not be less than 0: " + margin)
    } yield new OwnContributionLowerBound(bound, margin)
  }
}