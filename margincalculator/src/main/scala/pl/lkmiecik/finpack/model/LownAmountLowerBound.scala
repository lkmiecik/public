package pl.lkmiecik.finpack.model

import pl.lkmiecik.finpack.util.EitherConstructorByCondition

case class LownAmountLowerBound private(val bound: Double, private val ownContributionLowerBoundsList: List[OwnContributionLowerBound]) {
  
  val ownContributionLowerBounds: Option[List[OwnContributionLowerBound]] = {
    Option(ownContributionLowerBoundsList.sortBy(-_.bound))
  }
}

object LownAmountLowerBound extends EitherConstructorByCondition {
  
  def apply(bound: Double, ownContributionLowerBoundsList: List[OwnContributionLowerBound]): Nothing = {
    throw new IllegalArgumentException("Use different construction arguments")
  }
  
  def apply(bound: Double, marginList: List[Double], ownContributionBounds: List[Double]): Either[String, LownAmountLowerBound] = {
    for {
      c1 <- either(bound >= 0, 
          "Lown amount lower bound should not be less than 0: " + bound)
      c2 <- either(!ownContributionBounds.isEmpty, 
          "Own contribution list should not be empty")
      c3 <- either(marginList.size == ownContributionBounds.size, 
          "Margins list size and own contribution list size not the same: " + marginList.size + " != " + ownContributionBounds.size)
      ownContributionLowerBoundsList <- listOfEitherToEitherList(marginList.zip(ownContributionBounds).map(t => OwnContributionLowerBound(t._2, t._1)))
    } yield new LownAmountLowerBound(bound, ownContributionLowerBoundsList)
  }
  
  
  private def listOfEitherToEitherList(list: List[Either[String, OwnContributionLowerBound]]): Either[String, List[OwnContributionLowerBound]] = {
    list.partition(_.isLeft) match {
      case (Nil, ownBounds) => Right(for(Right(bound) <- ownBounds) yield bound)
      case (strings, _) => Left((for{Left(s) <- strings} yield s).head)
    }
  }
}