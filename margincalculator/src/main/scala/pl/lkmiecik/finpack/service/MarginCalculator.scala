package pl.lkmiecik.finpack.service

import pl.lkmiecik.finpack.model.LownPeriodLowerBound
import pl.lkmiecik.finpack.model.LownAmountLowerBound
import pl.lkmiecik.finpack.model.OwnContributionLowerBound
import scala.annotation.tailrec
import scala.util.Try
import pl.lkmiecik.finpack.model.PLN
import pl.lkmiecik.finpack.model.Percent
import pl.lkmiecik.finpack.model.ColumnForAmountInterval
import pl.lkmiecik.finpack.util.EitherConstructorByCondition


class MarginCalculator(private val lownPeriodsList: List[LownPeriodLowerBound] = List()) extends EitherConstructorByCondition {
  
  private val lownPeriods: Option[List[LownPeriodLowerBound]] = {
    Option(lownPeriodsList.sortBy(-_.boundInMonths))
  }
  
  def calculate(month: Int, lownAmount: PLN, ownContribution: Percent): Option[Double] = {
    
    if (!isDataAvailable(lownPeriods) || isArgumentsInvalid(month, lownAmount.value, ownContribution.value) ) {
      Option.empty
    } else {
      lownPeriods
        .flatMap(periods => findCorrectPeriod(periods, month))
        .flatMap(period => period.lownAmountLowerBounds)
        .flatMap(amountBounds => findCorrectLownAmountLowerBound(amountBounds, lownAmount.value))
        .flatMap(amountBound => amountBound.ownContributionLowerBounds)
        .flatMap(contributionBounds => findCorrectOwnContributionLowerBound(contributionBounds, ownContribution.value))
        .flatMap(contributionBound => Option(contributionBound.margin))
    }
  }
  
  private def isDataAvailable(data: Option[List[LownPeriodLowerBound]]): Boolean = {
    data.map(_.size > 0).getOrElse(false)
  }
  
  private def isArgumentsInvalid(month: Int, lownAmount: Double, ownContribution: Double) = {
    month <= 0 || lownAmount <= 0 || ownContribution < 0 || ownContribution >= 100
  }
  
  private def findCorrectPeriod(lownPeriodsList: List[LownPeriodLowerBound], month: Int): Option[LownPeriodLowerBound] = {
    val period = lownPeriodsList
      .filter(period => period.boundInMonths <= month)
      .headOption
    period
  }
  
  private def findCorrectLownAmountLowerBound(
      lownAmountLowerBounds: List[LownAmountLowerBound], lownAmount: Double): Option[LownAmountLowerBound] = {
    val amountBound = lownAmountLowerBounds
      .filter(lowerBound => lowerBound.bound <= lownAmount)
      .headOption
    amountBound
  }
  
  private def findCorrectOwnContributionLowerBound(
      ownContributionLowerBounds: List[OwnContributionLowerBound], ownContribution: Double): Option[OwnContributionLowerBound] = {
    val ownContributionBound = ownContributionLowerBounds
      .filter(lowerBound => lowerBound.bound <= ownContribution)
      .headOption
    ownContributionBound
  }

  def addNextPeriod(
        lowerBoundInMonths: Int, marginTable: List[ColumnForAmountInterval], 
        amountBounds: List[PLN], ownContributionBounds: List[Percent]
      ): Either[String, MarginCalculator] = {
    
    for {
      bound <- LownPeriodLowerBound(lowerBoundInMonths, marginTable, amountBounds, ownContributionBounds)
      isValid <- either(boundsInMonthsNotRepeated(bound.boundInMonths, lownPeriodsList), 
              "Bounds in months should be different with one another")
    } yield new MarginCalculator(bound :: lownPeriodsList)
  }
  
  private def boundsInMonthsNotRepeated(boundInMonths: Int, lownPeriodsList: List[LownPeriodLowerBound]): Boolean = lownPeriodsList match {
    case List() => true
    case list => {
          list.filter(_.boundInMonths == boundInMonths).size == 0
    }
  }
  
}

object MarginCalculator {
  def apply(boundInMonths: Int, marginTable: List[ColumnForAmountInterval], 
      amountBounds: List[PLN], ownContributionBounds: List[Percent]): Either[String, MarginCalculator] = {
    val calculator = new MarginCalculator
    calculator.addNextPeriod(boundInMonths, marginTable, amountBounds, ownContributionBounds)
  }
}
